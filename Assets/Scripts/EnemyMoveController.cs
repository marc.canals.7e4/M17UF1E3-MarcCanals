﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoveController : MonoBehaviour
{
    private GameObject player;
    private Transform target;
    private Vector3 posPlayer, posEnemy;
    private float speed;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        target = player.GetComponent<Transform>();
        speed = 10;
    }

    // Update is called once per frame
    void Update()
    {
        posPlayer = target.transform.position;
        posEnemy = transform.position;

        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(posEnemy, posPlayer, step);

        if (Vector3.Distance(posEnemy, posPlayer) < 0.001f)
        {
            target.position *= -1.0f;
        }

    }
}
