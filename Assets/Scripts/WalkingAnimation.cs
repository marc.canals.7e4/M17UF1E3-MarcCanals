﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingAnimation : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Transform transforma;
    public Sprite[] sprites;
    private int animationState = 0, weight;
    private float speed, distance;
    private Vector3 posFinal, posActual, posInicial;
    private bool reachedPos = false;
    // Fa el exchange de sprites, l'animació basicament
    void ChangeSprite()
    {
        if (animationState <= sprites.Length-1)
        {
            animationState++;
        }
        else
        {
            animationState = 0;
        }

        if (animationState >= 0 && animationState <= sprites.Length-1)
        {
            spriteRenderer.sprite = sprites[animationState];
        }
    }
    //Patrolling
    void Movement(int direction, Vector3 posToReach)
    {
        if (direction == 1)
        {
            if (posActual.x < posToReach.x)
            {
                transforma.transform.position = transforma.transform.position + new Vector3(1 + direction * (speed * 3) * Time.deltaTime, 0 * (speed * 3) * Time.deltaTime);               
            }
            else
            {
                spriteRenderer.flipX = false;
                reachedPos = true;
            }
        }
        else
        {
            if (posActual.x > posToReach.x)
            {
                transforma.transform.position = transforma.transform.position + new Vector3(-1 + direction * (speed*3) * Time.deltaTime, 0 * (speed * 3) * Time.deltaTime);
            }
            else
            {
                spriteRenderer.flipX = true;
                reachedPos = false;
            }
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        Application.targetFrameRate = 12;
        transforma = gameObject.GetComponent<Transform>();
        speed = gameObject.GetComponent<DataPlayer>().speed;
        distance = gameObject.GetComponent<DataPlayer>().distanceToDo;
        weight = gameObject.GetComponent<DataPlayer>().weight;
        posFinal = transform.transform.position + new Vector3(transforma.transform.position.x + distance, 0);
        posInicial = transforma.transform.position;

        //Inversament proporcional formula per determina velocitat
        speed = speed / (weight * 10); // 10 -> CHANGE TO MODIFY SPEED / 10 ATLEAST
    }

    // Update is called once per frame
    void Update()
    {
        posActual = transforma.transform.position;
        if (!reachedPos)
        {
            ChangeSprite();
            Movement(1,posFinal);
        } else
        {
            ChangeSprite();
            Movement(-1,posInicial);
        }

    }
}
