﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySlayer : MonoBehaviour
{
    private int enemies, points;
    private DataPlayer player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").gameObject.GetComponent<DataPlayer>();
        enemies = player.GetComponent<DataPlayer>().enemies;
        points = player.GetComponent<DataPlayer>().points;


    }
    private void OnMouseDown()
    {
        Destroy(this.gameObject);
        enemies--;
        points += 5;
        player.GetComponent<DataPlayer>().enemies = enemies;
        player.GetComponent<DataPlayer>().points = points;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
