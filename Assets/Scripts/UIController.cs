﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    private int numVides, numEnemies, numPoints;
    private Text framerate, vides, countEnemies, points;
    private GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        countEnemies = GameObject.Find("ENEMIES").GetComponent<Text>();
        points = GameObject.Find("POINTS").GetComponent<Text>();
        framerate = GameObject.Find("FPS").GetComponent<Text>();
        vides = GameObject.Find("VIDES").GetComponent<Text>();
        numVides = player.GetComponent<DataPlayer>().lives;
    }

    // Update is called once per frame
    void Update()
    {
        numEnemies = player.GetComponent<DataPlayer>().enemies;
        numPoints = player.GetComponent<DataPlayer>().points;
        numVides = player.GetComponent<DataPlayer>().lives;
        framerate.text = "FPS: " + Mathf.FloorToInt(1f / Time.deltaTime);
        vides.text = "VIDES: " + numVides;
        points.text = "PUNTS: " + numPoints;
        countEnemies.text = "ENEMICS VIUS: " + numEnemies;
    }
}
