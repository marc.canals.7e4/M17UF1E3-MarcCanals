﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyCollitionControler : MonoBehaviour
{
    //Collition controller with enemies and player
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Debug.Log("Collition with Player - TRUE");
            GameObject player = collision.gameObject;
            int vides = player.GetComponent<DataPlayer>().lives;
            int enemies = player.GetComponent<DataPlayer>().enemies;
            int points = player.GetComponent<DataPlayer>().points;

            if (vides == 0)
            {
                Destroy(player);
                SceneManager.LoadScene(0);
            }
            else
            {
                points += 5;
                vides--;
                enemies--;
                player.GetComponent<DataPlayer>().enemies = enemies;
                player.GetComponent<DataPlayer>().lives = vides;
                player.GetComponent<DataPlayer>().points = points;
            }
        }
        Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
