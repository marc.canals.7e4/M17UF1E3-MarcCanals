﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiantAbility : MonoBehaviour
{
    private float startGiantTime, currentTime, cooldownTime = 15.0f, height, distance;
    private Transform transforma;
    private DataPlayer player;
    private SpriteRenderer sprite;
    private Vector3 scaleMax, scaleChanger, posFinal, initScale;

    public enum GiantStates
    {
        Idle,
        Growing,
        GiantState,
        Ungrowing,
        Cooldown,
    }

    public GiantStates currentState;
    // Start is called before the first frame update

    void Start()
    {
        transforma = gameObject.GetComponent<Transform>();
        player = gameObject.GetComponent<DataPlayer>();
        height = gameObject.GetComponent<DataPlayer>().height;
        distance = gameObject.GetComponent<DataPlayer>().distanceToDo;
        sprite = gameObject.GetComponent<SpriteRenderer>();
        initScale = sprite.transform.localScale;
        scaleMax = new Vector3(sprite.transform.localScale.x + height * 2, sprite.transform.localScale.y + height * 2);
        posFinal = transform.transform.position + new Vector3(transforma.transform.position.x + distance, 0);
    }
   
    //States func
    void GrowPlayer()
    {
        startGiantTime = Time.time;
        scaleChanger = new Vector3(1f, 1f, 0);
        if (sprite.transform.localScale.x <= scaleMax.x)
        {
            sprite.transform.localScale += scaleChanger;
        }
        else
        {
            currentState = GiantStates.GiantState;
        }
    }
    void GiantState()
    {
        if (transforma.transform.position.x >= posFinal.x)
        {
            currentState = GiantStates.Ungrowing;
        }
    }
    void UngrowPlayer()
    {
        scaleChanger = new Vector3(-1.5f, -1.5f, 0);
        if (sprite.transform.localScale.x >= initScale.x)
        {
            sprite.transform.localScale += scaleChanger;
        }
        else
        {
            currentState = GiantStates.Cooldown;
        }
    }
    void Cooldown()
    {

        if ((currentTime - startGiantTime) >= cooldownTime)
        {
            currentState = GiantStates.Idle;
        }
    }
    void IdleState()
    {

    }
    // Update is called once per frame
    void Update()
    {
        currentTime = Time.time;
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            currentState = GiantStates.Growing;
        }
        switch (currentState)
        {
            case GiantStates.Growing:
                GrowPlayer();
                break;
            case GiantStates.GiantState:
                GiantState();
                break;
            case GiantStates.Ungrowing:
                UngrowPlayer();
                break;
            case GiantStates.Cooldown:
                Cooldown();
                break;
            case GiantStates.Idle:
                IdleState();
                break;

        }
    }
}
