﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerType
{
    Barbarian,
    Sorcerer,
    Knight,
    Bard,
    Assasin

}

public class DataPlayer : MonoBehaviour
{
    public string name, subname;
    public float height, speed, distanceToDo;
    public int weight, lives, points, enemies;
    public PlayerType playerType;
    // Start is called before the first frame update
    
    void Start()
    {
        enemies = 0;
        points = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
