﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy;
    private GameObject player;
    private int enemiesGenerated = 0;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        InvokeRepeating("CreateEnemies", 0.4f, 1);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void CreateEnemies()
    {
        enemiesGenerated = player.GetComponent<DataPlayer>().enemies;
        Instantiate(enemy, new Vector3(Random.Range(-45, 175), Random.Range(-55, 55), 0), Quaternion.identity);
        enemiesGenerated++;
        player.GetComponent<DataPlayer>().enemies = enemiesGenerated;
    }
}
